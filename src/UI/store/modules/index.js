import player from './player'
import shikimori from './shikimori'

export default {
  player,
  shikimori
}