/**
 * @type {vuex.Player}
 */
export default {
  episodes: [],
  currentEpisode: null,
  currentTranslation: null,
}