# Play Шикимори Online 
![Chrome Web Store](https://img.shields.io/chrome-web-store/rating/eopmgkejoplocjnpljjhgbeadjoomcbd?label=%D0%A0%D0%B5%D0%B9%D1%82%D0%B8%D0%BD%D0%B3&style=flat) 
![Chrome Web Store](https://img.shields.io/chrome-web-store/users/eopmgkejoplocjnpljjhgbeadjoomcbd?label=%D0%9F%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D0%B5%D0%B9&style=flat)
![Chrome Web Store](https://img.shields.io/chrome-web-store/v/eopmgkejoplocjnpljjhgbeadjoomcbd?label=%D0%92%D0%B5%D1%80%D1%81%D0%B8%D1%8F&style=flat)

| ![Внешний вид кнопки "Смотреть онлайн"](promo/watch-button.png) | ![Внешний вид плеера](promo/player.png) | ![Внешний вид плеера](promo/comments.png)  |
| ------------- |:-------------:| -----:|

Это браузерное расширение, которое позволяет вам смотреть аниме онлайн и синхронизировать его с вашим списком на Шикимори.


## Установка

* [Google Chrome](https://chrome.google.com/webstore/detail/play-shikimori-online/eopmgkejoplocjnpljjhgbeadjoomcbd)
* [Firefox](https://addons.mozilla.org/firefox/addon/play-shikimori/)
* [Opera](https://addons.opera.com/ru/extensions/details/play-shikimori-beta/)
* [Other](https://chrome.google.com/webstore/detail/play-shikimori-online/eopmgkejoplocjnpljjhgbeadjoomcbd)

## Возможности

В разработке этого расширения основной упор делается непосредственно на онлайн просмотре и всего что с ним связано. Моя цель — сделать его настолько удобным, насколько это возможно.

* Вам не нужно регистрироваться чтобы смотреть аниме онлайн.
* Новые серии добавляются в тот же миг, как они появляются на хостинге-видео. 
* Плеер умеет самостоятельно переключаться на следующую серию, когда текущая подходит к концу.
* Плеер запоминает время, на котором вы остановили просмотр серии, и возобновляет воспроизведение с этого же места.
* Вы можете начать просмотр серии на одном устройстве, а продолжить на другом. Время на котором вы остановились синхронизируется между всеми вашими устройствами
* Ведётся учет в каком переводе вы смотрите. Благодаря этому, когда вы открываете новый сериал, доступен более интелектуальный выбор переводов на основе всех ваших предпочтений.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

```bash
# Setup environment
NODE_ENV=production
BROWSER=chrome # or firefox

# Shikimori oauth
SHIKIMORI_REDIRECT_URI=
SHIKIMORI_CLIENT_ID=
SHIKIMORI_CLIENT_SECRET=

# Run build
npm run build
```

## License
[GPL-3.0](https://github.com/cawa-93/play-shikimori-online/blob/master/LICENSE)